// Copyright (c) 2021, BlockProject 3D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 3D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::{
    convert::From,
    ops::{Deref, Index},
    string::{String, ToString}
};

use super::encoding::{decode_str, encode_str};
use crate::{Utf32Char, Utf32Str};

pub struct Utf32String
{
    data: Vec<Utf32Char>
}

impl Utf32String
{
    pub fn len(&self) -> usize
    {
        return self.data.len();
    }

    pub fn new(s: &Utf32Str) -> Utf32String
    {
        let mut v = Vec::new();
        for adwik in s {
            v.push(*adwik);
        }
        return Utf32String { data: v };
    }

    pub fn find(&self, ch: Utf32Char) -> Option<usize>
    {
        for i in 0..self.data.len() {
            if self.data[i] == ch {
                return Some(i);
            }
        }
        return None;
    }

    pub fn as_bytes(&self) -> &[u8]
    {
        unsafe {
            return std::mem::transmute(self.data.deref());
        }
    }

    pub fn as_ptr(&self) -> *const Utf32Char
    {
        return self.data.as_ptr();
    }
}

impl From<String> for Utf32String
{
    fn from(s: String) -> Self
    {
        return Utf32String {
            data: encode_str(s.as_bytes())
        };
    }
}

impl From<&str> for Utf32String
{
    fn from(s: &str) -> Self
    {
        return Utf32String {
            data: encode_str(s.as_bytes())
        };
    }
}

impl Deref for Utf32String
{
    type Target = Utf32Str;

    fn deref(&self) -> &Utf32Str
    {
        return &self.data;
    }
}

impl Index<usize> for Utf32String
{
    type Output = Utf32Char;

    fn index(&self, index: usize) -> &Utf32Char
    {
        return &self.data[index];
    }
}

impl ToString for Utf32String
{
    fn to_string(&self) -> String
    {
        let v = decode_str(&self.data);
        unsafe {
            return String::from_utf8_unchecked(v);
        }
    }
}

impl From<Utf32String> for String
{
    fn from(s: Utf32String) -> Self
    {
        let v = decode_str(&s.data);
        unsafe {
            return String::from_utf8_unchecked(v);
        }
    }
}

#[cfg(test)]
mod tests
{
    use crate::string::Utf32String;

    #[test]
    fn encode_string()
    {
        let u = Utf32String::from("🗻∈🌏");
        let s: String = u.into();
        assert_eq!(&s, "🗻∈🌏");
    }
}
