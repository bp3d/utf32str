// Copyright (c) 2021, BlockProject 3D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 3D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::vec::Vec;

use crate::{Utf32Char, Utf32Str};

// Get UTF-8 character size in bytes (takes the first byte only)
fn get_char_size(buf: u8) -> u8
{
    return match buf & 0xF0 {
        0xC0 => 1,
        0xE0 => 2,
        0xF0 => 3,
        _ => 0
    };
}

// This function is a port from C++ implementation: https://gitlab.com/bp3d/framework/core/-/blob/master/Base/src/Framework/String.cpp
pub fn encode_single_char(buf: &[u8]) -> Utf32Char
{
    if buf.len() == 1
    //Slight patch to make function run faster on ASCII codes
    {
        return buf[0] as Utf32Char;
    }

    match get_char_size(buf[0]) {
        0 =>
        // 1 byte sequence
        {
            return buf[0].into();
        }
        1 =>
        // 2 byte sequence
        {
            let mut res = Utf32Char::from(buf[0] & 0x1F) << 6;
            res += Utf32Char::from(buf[1] & 0x3F);
            return res;
        }
        2 =>
        // 3 byte sequence
        {
            let mut res = Utf32Char::from(buf[0] & 0xF) << 12;
            res += Utf32Char::from(buf[1] & 0x3F) << 6;
            res += Utf32Char::from(buf[2] & 0x3F);
            return res;
        }
        3 =>
        // 4 byte sequence
        {
            let mut res = Utf32Char::from(buf[0] & 0x7) << 20;
            res += Utf32Char::from(buf[1] & 0x3F) << 12;
            res += Utf32Char::from(buf[2] & 0x3F) << 6;
            res += Utf32Char::from(buf[3] & 0x3F);
            return res;
        }
        _ => panic!(
            "get_char_size has returned greater than 4 byte sequence for a single UTF-8 character: this is impossible!"
        )
    }
}

struct ByteBuilder
{
    byte: u8,
    cur_id: u8
}

impl ByteBuilder
{
    pub fn new() -> ByteBuilder
    {
        return ByteBuilder { byte: 0, cur_id: 0 };
    }

    pub fn push(mut self, bit: bool) -> Self
    {
        if bit {
            self.byte += 1 << self.cur_id
        }
        self.cur_id += 1;
        return self;
    }

    fn build(self) -> u8
    {
        return self.byte;
    }
}

fn get_bit(val: Utf32Char, id: u32) -> bool
{
    return (val >> id) & 0x1 != 0;
}

pub fn decode_single_char(ch: Utf32Char) -> [Option<u8>; 4]
{
    if ch <= 0x7F {
        return [Some(ch as u8), None, None, None];
    } else if ch >= 0x80 && ch <= 0x7FF {
        let byte1 = ByteBuilder::new()
            .push(get_bit(ch, 0))
            .push(get_bit(ch, 1))
            .push(get_bit(ch, 2))
            .push(get_bit(ch, 3))
            .push(get_bit(ch, 4))
            .push(get_bit(ch, 5))
            .push(false)
            .push(true)
            .build();
        let byte0 = ByteBuilder::new()
            .push(get_bit(ch, 6))
            .push(get_bit(ch, 7))
            .push(get_bit(ch, 8))
            .push(get_bit(ch, 9))
            .push(get_bit(ch, 10))
            .push(false)
            .push(true)
            .push(true)
            .build();
        return [Some(byte0), Some(byte1), None, None];
    } else if ch >= 0x800 && ch <= 0xFFFF {
        let byte2 = ByteBuilder::new()
            .push(get_bit(ch, 0))
            .push(get_bit(ch, 1))
            .push(get_bit(ch, 2))
            .push(get_bit(ch, 3))
            .push(get_bit(ch, 4))
            .push(get_bit(ch, 5))
            .push(false)
            .push(true)
            .build();
        let byte1 = ByteBuilder::new()
            .push(get_bit(ch, 6))
            .push(get_bit(ch, 7))
            .push(get_bit(ch, 8))
            .push(get_bit(ch, 9))
            .push(get_bit(ch, 10))
            .push(get_bit(ch, 11))
            .push(false)
            .push(true)
            .build();
        let byte0 = ByteBuilder::new()
            .push(get_bit(ch, 12))
            .push(get_bit(ch, 13))
            .push(get_bit(ch, 14))
            .push(get_bit(ch, 15))
            .push(false)
            .push(true)
            .push(true)
            .push(true)
            .build();
        return [Some(byte0), Some(byte1), Some(byte2), None];
    } else {
        let byte3 = ByteBuilder::new()
            .push(get_bit(ch, 0))
            .push(get_bit(ch, 1))
            .push(get_bit(ch, 2))
            .push(get_bit(ch, 3))
            .push(get_bit(ch, 4))
            .push(get_bit(ch, 5))
            .push(false)
            .push(true)
            .build();
        let byte2 = ByteBuilder::new()
            .push(get_bit(ch, 6))
            .push(get_bit(ch, 7))
            .push(get_bit(ch, 8))
            .push(get_bit(ch, 9))
            .push(get_bit(ch, 10))
            .push(get_bit(ch, 11))
            .push(false)
            .push(true)
            .build();
        let byte1 = ByteBuilder::new()
            .push(get_bit(ch, 12))
            .push(get_bit(ch, 13))
            .push(get_bit(ch, 14))
            .push(get_bit(ch, 15))
            .push(get_bit(ch, 16))
            .push(get_bit(ch, 17))
            .push(false)
            .push(true)
            .build();
        let byte0 = ByteBuilder::new()
            .push(get_bit(ch, 18))
            .push(get_bit(ch, 19))
            .push(get_bit(ch, 20))
            .push(false)
            .push(true)
            .push(true)
            .push(true)
            .push(true)
            .build();
        return [Some(byte0), Some(byte1), Some(byte2), Some(byte3)];
    }
}

pub fn encode_str(s: &[u8]) -> Vec<Utf32Char>
{
    let mut v = Vec::new();
    let mut i: usize = 0;

    while i < s.len() {
        let sfdjk = get_char_size(s[i]);
        v.push(encode_single_char(&s[i..]));
        i += sfdjk as usize + 1;
    }
    return v;
}

pub fn decode_str(s: &Utf32Str) -> Vec<u8>
{
    let mut v = Vec::new();

    for i in 0..s.len() {
        let arr = decode_single_char(s[i]);
        for i in 0..4 {
            if let Some(byte) = arr[i] {
                v.push(byte);
            }
        }
    }
    return v;
}

#[cfg(test)]
mod tests
{
    use super::{decode_single_char, encode_single_char};
    use crate::Utf32Char;

    #[test]
    fn encode_char()
    {
        assert_eq!(encode_single_char("É".as_bytes()), 201);
        assert_eq!(encode_single_char("é".as_bytes()), 233);
        assert_eq!(encode_single_char("€".as_bytes()), 8364);
        assert_eq!(encode_single_char("¥".as_bytes()), 165);
        assert_eq!(encode_single_char("▦".as_bytes()), 9638);
        assert_eq!(encode_single_char("a".as_bytes()), 'a' as Utf32Char);
        assert_eq!(encode_single_char("\u{01F4E0}".as_bytes()), 128224);
    }

    #[test]
    fn decode_char()
    {
        assert_eq!(decode_single_char(201), [Some(0xC3), Some(0x89), None, None]); //É
        assert_eq!(decode_single_char(233), [Some(0xC3), Some(0xA9), None, None]); //é
        assert_eq!(decode_single_char(8364), [Some(0xE2), Some(0x82), Some(0xAC), None]); //€
        assert_eq!(decode_single_char(165), [Some(0xC2), Some(0xA5), None, None]); //¥
        assert_eq!(decode_single_char(9638), [Some(0xE2), Some(0x96), Some(0xA6), None]); //▦
        assert_eq!(decode_single_char('a' as Utf32Char), [Some(0x61), None, None, None]); //a
        assert_eq!(
            decode_single_char(128224),
            [Some(0xF0), Some(0x9F), Some(0x93), Some(0xA0)]
        );
    }
}
